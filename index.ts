import express from "express";
import { CONFIG } from "./config";
import backOfficeRouter from "./src/backoffice/back-office.route";
const cors = require('cors')

const app = express();
app.use(cors('*'));
app.use(express.json());

//assign base path
app.use("/vive/back-office", backOfficeRouter);

//default port
const port = CONFIG.localPort;
app.listen(port, () => console.log("listen in port 9000"));

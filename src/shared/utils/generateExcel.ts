import excel from "exceljs";
var path = require("path");

export const downloadExcel = (res: any) => {
  const basePath = "./excelTemplate.xlsx";
  try {
    var workbook = new excel.Workbook();
    var worksheet = workbook.addWorksheet();

    worksheet.columns = [
      { header: "Id", key: "id", width: 10 },
      { header: "Name", key: "name", width: 32 },
      { header: "D.O.B.", key: "DOB", width: 10 },
    ];
    worksheet.addRow({ id: 1, name: "John Doe", DOB: new Date(1970, 1, 1) });
    worksheet.addRow({ id: 2, name: "Jane Doe", DOB: new Date(1965, 1, 7) });

    workbook.xlsx
      .writeFile("excelTemplate.xlsx")
      .then((response) => {
        res.sendFile(path.resolve(basePath));
      })
      .catch((err) => {
        console.log(err);
      });
  } catch (err) {
    console.log("OOOOOOO this is the error: " + err);
  }
};

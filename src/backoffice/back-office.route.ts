import { Router } from "express";
import * as path from "path";
import { downloadExcel } from "../shared/utils/generateExcel";
import { CategoriesPost } from "./domain/categories";

const router = Router();

const mockBasePath = "./src/backoffice/mocks";

router.get("/categories", (req, res) => {
  return res.sendFile(path.resolve(`${mockBasePath}/categories.json`));
});

router.post("/categories", (req, res) => {
  const { name, color } = req.body as CategoriesPost;
  if (!Boolean(name) || !Boolean(color)) {
    return res.status(404).json({
      message: "name y color son parametros obligatorios",
    });
  }

  return res.status(200).json({
    results: "ok categoría registrada",
  });
});

router.put("/categories/:id", (req, res) => {
  if (!req.params.id) {
    return res.status(404).json({
      message: "el parametro id es obligatorio en la url /categories/${id}",
    });
  }

  const { name, color } = req.body as CategoriesPost;
  if (!Boolean(name) || !Boolean(color)) {
    return res.status(404).json({
      message: "name, color  son parametros obligatorios",
    });
  }

  return res.status(200).json({
    results: "ok categoría actualizada",
  });
});

router.get("/sub-categories", (req, res) => {
  return res.sendFile(path.resolve(`${mockBasePath}/subCategories.json`));
});

router.post("/sub-categories", (req, res) => {
  const { category_id, name, color } = req.body;
  if (!Boolean(category_id) || !Boolean(name) || !Boolean(color)) {
    return res.status(404).json({
      message: "category_id, name, color son parametros obligatorios",
    });
  }

  return res.status(200).json({
    results: "ok sub categoría registrada",
  });
});

router.put("/sub-categories/:id", (req, res) => {
  if (!req.params.id) {
    return res.status(404).json({
      message: "el parametro id es obligatorio en la url /sub-categories/${id}",
    });
  }

  const { category_id, name, color } = req.body;
  if (!Boolean(category_id) || !Boolean(name) || !Boolean(color)) {
    return res.status(404).json({
      message: "category_id, name, color son parametros obligatorios",
    });
  }

  return res.status(200).json({
    results: "ok sub categoría actualizada",
  });
});

router.get("/providers", (req, res) => {
  return res.sendFile(path.resolve(`${mockBasePath}/providers.json`));
});

router.post("/providers", (req, res) => {
  return res.status(200).json({ results: "ok" });
});

router.put("/providers/:id", (req, res) => {
  if (!req.params.id) {
    return res.status(404).json({
      message: "el parametro id es obligatorio en la url /providers/${id}",
    });
  }

  return res.status(200).json({ results: "ok" });
});

router.get("/sales", (req, res) => {
  const { init_date, end_date } = req.query;
  if (!Boolean(init_date) || !Boolean(end_date)) {
    return res.status(404).json({
      message:
        "init_date y end_date son parametros obligatorios en formato YYYY-MM-DD",
    });
  }

  return res.sendFile(path.resolve(`${mockBasePath}/salesHistory-es.json`));
});

router.get("/sales/download", (req, res) => {
  const { init_date, end_date } = req.query;
  if (!Boolean(init_date) || !Boolean(end_date)) {
    return res.status(404).json({
      message:
        "init_date y end_date son parametros obligatorios en formato YYYY-MM-DD",
    });
  }

  return downloadExcel(res);
});

router.get("/earns", (req, res) => {
  const { init_date, end_date } = req.query;

  if (!Boolean(init_date) || !Boolean(end_date)) {
    return res.status(404).json({
      message:
        "init_date y end_date son parametros obligatorios en formato YYYY-MM-DD",
    });
  }

  return res.sendFile(path.resolve(`${mockBasePath}/earns.json`));
});

router.get("/earns/download", (req, res) => {
  const { init_date, end_date } = req.query;

  if (!Boolean(init_date) || !Boolean(end_date)) {
    return res.status(404).json({
      message:
        "init_date y end_date son parametros obligatorios en formato YYYY-MM-DD",
    });
  }
  return downloadExcel(res);
});

export default router;

export interface CategoriesPost {
  id?: number;
  name: string;
  color: string;
}
